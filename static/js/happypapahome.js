/*
 * The Happy Papa Home Blog JS
 * Copyright (c) 2018 Daniel Widyanto <daniel.widyanto@kwaa.name>
 * 
 */

/* Register all components --------------------------------------------------*/
Vue.component('card', {
	props: ['image', 'title', 'link'],
	template: 
		`<div class="card">` +
		`	<img v-if="image" :src="image" />` +
		`	<h3>{{ title }}</h3>` +
		`	<slot></slot>` +
		`</div>`,
});

Vue.component('postcard', {
	props: ['image', 'date', 'title', 'link'],
	template: 
		`<div class="postcard">` +
		`	<img v-if="image" :src="image" />` +
		`	<h3><a :href="link">{{ title }}</a></h3>` +
		`	<slot></slot>` +
		`	<p v-if="date" class="date"><i class="fa fa-calendar-check-o"></i> {{ date }}</p>` +
		`	<slot name="tags"></slot>` +
		`</div>`,
});

Vue.component('post', {
	props: ['image', 'date', 'title', 'prev_title', 'prev_link', 'next_title', 'next_link'],
	template:
		`<div class="post">` +
		`	<img v-if="image" :src="image" class="featured-img" />` +
		`	<h3>{{ title }}</h3>` +
		`	<p v-if="date" class="date"><i class="fa fa-calendar-check-o"></i> {{ date }}</p>` +
		`	<slot></slot>` +
		`	<slot name="tags"></slot>` +
		`	<nav>` +
		`		<h4 v-if="prev_title"><i class="fa fa-arrow-left" aria-hidden="true"></i> <a :href="prev_link">{{ prev_title }}</a></h4>` +
		`		<h4 v-if="next_title"><a :href="next_link">{{ next_title }}</a> <i class="fa fa-arrow-right" aria-hidden="true"></i></h4>` +
		'	</nav>' +
		`</div>`,
});

/* Create root instance -----------------------------------------------------*/
new Vue({
  el: '#app',
});
